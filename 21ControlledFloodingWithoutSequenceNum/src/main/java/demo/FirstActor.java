package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);


	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			return new FirstActor();
		});
	}
	public static class List implements Serializable{
		private static final long serialVersionUID = 1L;
		public final ArrayList<ActorRef> list;
		public List(ArrayList<ActorRef> list){
			this.list = list;
		}
		public void add(ActorRef ref){
			list.add(ref);
		} 
		public ArrayList<ActorRef> getList(){
			return this.list;
		}
		public Integer len(){
			return this.list.size();
		}
	}
	static public class MyMessage{
		public final String data;
		public MyMessage(String data) {
			this.data = data;
		}
		public String getMessage(){
			return data;
		}
		
	}
	
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof List){
			List list = (List)message;
			log.info("["+getSelf().path().name()+"] received list"+list+ " from ["+ getSender().path().name() +"]");
			MyMessage m =new MyMessage("hi");
			for(int i=0;i<list.len();i++){
				ArrayList<ActorRef> listref= list.getList();
				ActorRef ref = listref.get(i);
				log.info(getSelf().path().name()+" send a message " + m.data+ " to " + ref.path().name());
				ref.tell(m, getSelf());
			}
		}
	}


}
