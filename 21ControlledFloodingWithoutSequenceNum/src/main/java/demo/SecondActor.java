package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.ArrayList;
import demo.FirstActor.List;
import demo.FirstActor.MyMessage;

public class SecondActor extends UntypedAbstractActor {
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public List list;

	// Empty Constructor
	public SecondActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(SecondActor.class, () -> {
			return new SecondActor();
		});
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof List){
			list = (List)message;
			log.info("["+getSelf().path().name()+"] received list"+list+ " from ["+ getSender().path().name() +"]");		
		}
		if (message instanceof MyMessage){
			MyMessage m = (MyMessage)message;
			for(int i=0;i<list.len();i++){
				ArrayList<ActorRef> listref= list.getList();
				ActorRef ref = listref.get(i);
				log.info(getSelf().path().name()+" send a message " + m.data+ " to " + ref.path().name());
				ref.tell(m, getSelf());
			}
		} 
	}	
}
