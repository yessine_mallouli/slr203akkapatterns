package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import java.util.ArrayList;
import demo.FirstActor.List;
import demo.FirstActor.MyMessage;

/**
 * @author Remi SHARROCK and Axel Mathieu
 * @description Create an actor and passing his reference to
 *				another actor by message.
 */
public class ControlledFloodingWithoutSequenceNum {

	public static void main(String[] args) {
		// Instantiate an actor system
		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
	    final ActorRef a = system.actorOf(FirstActor.createActor(), "a");
	    final ActorRef b = system.actorOf(SecondActor.createActor(), "b");
		final ActorRef c = system.actorOf(ThirdActor.createActor(), "c");
		final ActorRef d = system.actorOf(FourthActor.createActor(), "d");
		final ActorRef ee = system.actorOf(FifthActor.createActor(), "ee");
	    final ArrayList<ActorRef> list1 = new ArrayList<ActorRef>();
		final ArrayList<ActorRef> list2 = new ArrayList<ActorRef>();
		final ArrayList<ActorRef> list3 = new ArrayList<ActorRef>();
		final ArrayList<ActorRef> list4 = new ArrayList<ActorRef>();
		final ArrayList<ActorRef> list5 = new ArrayList<ActorRef>();
		list1.add(b);
		list1.add(c);
		list2.add(d);
		list3.add(d);
		list4.add(ee);
		list5.add(b);

		List llist1 = new List(list1);
		List llist2 = new List(list2);
		List llist3 = new List(list3);
		List llist4 = new List(list4);
		List llist5 = new List(list5);

		//ccreating the adjacency matrix
	    a.tell(llist1, ActorRef.noSender());
	    b.tell(llist2, ActorRef.noSender());
		c.tell(llist3, ActorRef.noSender());
		d.tell(llist4, ActorRef.noSender());
		ee.tell(llist5, ActorRef.noSender());
		
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(1000);
	}

}
