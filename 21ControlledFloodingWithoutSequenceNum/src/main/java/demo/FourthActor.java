package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.ArrayList;
import demo.FirstActor.List;
import demo.FirstActor.MyMessage;

public class FourthActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public List list;
	public boolean B_is_here=false;
	public boolean C_is_here=false;
	public FourthActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FourthActor.class, () -> {
			return new FourthActor();
		});
	}


	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof List){
			list = (List)message;
			log.info("["+getSelf().path().name()+"] received list"+list+ " from ["+ getSender().path().name() +"]");	
		}
	
	if (message instanceof MyMessage){
		MyMessage m = (MyMessage)message;
		if (getSender().path().name()=="b"){B_is_here=true;
			log.info(getSelf().path().name()+" received message from "+ getSender().path().name());
		}else{C_is_here=true;
			log.info(getSelf().path().name()+" received message from "+ getSender().path().name());
		}
		if (B_is_here && C_is_here){

		for(int i=0;i<list.len();i++){
			ArrayList<ActorRef> listref= list.getList();
			ActorRef ref = listref.get(i);
			log.info(getSelf().path().name()+" send a message " + m.data+ " to " + ref.path().name());
			ref.tell(m, getSelf());
		}
		B_is_here = false;
		C_is_here = false;
	}
	}
	}


}
