package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.ArrayList;
import demo.FirstActor.List;
import demo.FirstActor.MyMessage;

public class FifthActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public boolean pass=true;
	public List list;
	public static ArrayList<Integer> checkList = new ArrayList<Integer>();
	public FifthActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FifthActor.class, () -> {
			checkList.add(4);

			return new FifthActor();
		});
	}
	


	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof List){
			list = (List)message;
			log.info("["+getSelf().path().name()+"] received list"+list+ " from ["+ getSender().path().name() +"]");		}
		if (message instanceof MyMessage){
				MyMessage am = (MyMessage)message;
				int seqNum = am.getSeqNum();
				log.info(getSelf().path().name()+ " received the sequence number "+seqNum);
				
				if(! checkList.contains(seqNum)){
					checkList.add(seqNum);
					MyMessage m =new MyMessage(am.data,checkList.get(0));

				if(pass==true){
					// the actor ee will send 1 message only to b
				for(int i=0;i<list.len();i++){
					ArrayList<ActorRef> listref= list.getList();
					ActorRef ref = listref.get(i);
					log.info(getSelf().path().name()+" send a message " + m.data+ " to " + ref.path().name());
					ref.tell(m, getSelf());
					pass=false;
				}
			}else{
				log.info("infinite loop confirmed!");
			}
		}
			
		}
	}

}
