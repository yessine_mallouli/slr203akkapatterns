package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public static ArrayList<Integer> checkList = new ArrayList<Integer>();


	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			checkList.add(0);

			return new FirstActor();
		});
	}
	public static class List implements Serializable{
		private static final long serialVersionUID = 1L;
		public final ArrayList<ActorRef> list;
		public List(ArrayList<ActorRef> list){
			this.list = list;
		}
		public void add(ActorRef ref){
			list.add(ref);
		} 
		public ArrayList<ActorRef> getList(){
			return this.list;
		}
		public Integer len(){
			return this.list.size();
		}
	}
	static public class MyMessage {
		public final String data;
		public int seqNum;
		public MyMessage(String data, int seqNum) {
			this.data = data;
			this.seqNum = seqNum;
		}
		public String getMessage(){
			return data;
		}
		public int getSeqNum(){
			return seqNum;
		}
		public void setSeqNum(int num){
			this.seqNum = num;
		}
		
	}
	
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof List){
			List list = (List)message;
			log.info("["+getSelf().path().name()+"] received list"+list+ " from ["+ getSender().path().name() +"]");
			MyMessage m =new MyMessage("hi",checkList.get(0));
			for(int i=0;i<list.len();i++){
				ArrayList<ActorRef> listref= list.getList();
				ActorRef ref = listref.get(i);
				log.info(getSelf().path().name()+" send a message " + m.data+ " to " + ref.path().name());
				ref.tell(m, getSelf());
			}
		}
	}


}
