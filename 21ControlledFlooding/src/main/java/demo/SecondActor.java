package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.ArrayList;
import demo.FirstActor.List;
import demo.FirstActor.MyMessage;

public class SecondActor extends UntypedAbstractActor {
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public List list;
	public static ArrayList<Integer> checkList = new ArrayList<Integer>();

	// Empty Constructor
	public SecondActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(SecondActor.class, () -> {
			checkList.add(1);

			return new SecondActor();
		});
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof List){
			list = (List)message;
			log.info("["+getSelf().path().name()+"] received list"+list+ " from ["+ getSender().path().name() +"]");		
		}
		if (message instanceof MyMessage){
			MyMessage am = (MyMessage)message;
			int seqNum = am.getSeqNum();
			log.info(getSelf().path().name()+ " received the sequence number "+seqNum);
			if(!checkList.contains(seqNum)){
			checkList.add(seqNum);
			MyMessage m =new MyMessage(am.data,checkList.get(0));
			for(int i=0;i<list.len();i++){
				ArrayList<ActorRef> listref= list.getList();
				ActorRef ref = listref.get(i);
				log.info(getSelf().path().name()+" send a message " + m.data+ " to " + ref.path().name());
				ref.tell(m, getSelf());
			}
		}
		} 
	}	
}
