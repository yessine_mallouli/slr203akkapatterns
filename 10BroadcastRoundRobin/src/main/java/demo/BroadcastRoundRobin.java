package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.FirstActor.MyMessage;
import demo.FirstActor.MyRefs;

/**
 * @author yessine MALLOULI
 * @description
 */
public class BroadcastRoundRobin {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first, transmitter and second actor 
        final ActorRef a = system.actorOf(FirstActor.createActor(), "a");
		final ActorRef b = system.actorOf(SecondActor.createActor(), "b");
		final ActorRef c = system.actorOf(ThirdActor.createActor(), "c");
        final ActorRef bc = system.actorOf(BroadcastActor.createActor(), "bc");	    
			// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
		
		MyRefs refs =new MyRefs(b,c);
		bc.tell(refs,ActorRef.noSender());
		a.tell(bc,ActorRef.noSender());
		b.tell(bc,ActorRef.noSender());
		c.tell(bc,ActorRef.noSender());
       
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
    }
    public static void sleepFor(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
