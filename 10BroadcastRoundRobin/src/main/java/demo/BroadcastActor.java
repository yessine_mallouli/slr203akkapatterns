package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;
import demo.FirstActor.MyRefs;

public class BroadcastActor extends UntypedAbstractActor {


	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    
    public ActorRef b;
    public ActorRef c;
    public BroadcastActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(BroadcastActor.class, () -> {
			return new BroadcastActor();
		});
	}
	
	/*public void changeRef(ActorRef actorRef){
		this.actorRef = actorRef;
		log.info("I was linked to actor reference {}", this.actorRef);
   }*/
  
	@Override
	public void onReceive(Object message) throws Throwable {
        if(message instanceof MyMessage){
            MyMessage m =(MyMessage)message;
            if (m.data=="yalla!"){
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
            b.tell(m,getSelf());
            c.tell(m,getSelf());
            log.info("["+getSelf().path().name()+"] sent message "+m.data);
            }
        }
        if(message instanceof MyRefs){
            MyRefs refs = (MyRefs)message;
            b=(ActorRef)refs.firstactorGetter();
            c=(ActorRef)refs.secondactorGetter();
        }
	}
	
	
}