package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import java.time.Duration;
import akka.event.LoggingAdapter;

public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public ActorRef ref;

	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			return new FirstActor();
		});
	}


	static public class MyMessage {
		public final String data;
	
		public MyMessage(String data) {
			this.data = data;
		}
	}
	static public class MyRefs {
		public final ActorRef a1;
		public final ActorRef a2;
		public MyRefs(ActorRef a1,ActorRef a2) {
			this.a1 = a1;
			this.a2 = a2;
		}
		public ActorRef firstactorGetter(){
			return a1;
		}
		public ActorRef secondactorGetter(){
			return a2;
		}
}
	
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof ActorRef){
			ref = (ActorRef)message;
			MyMessage m = new MyMessage("go");
			getContext().system().scheduler()
			.scheduleOnce(Duration.ofMillis(1000), getSelf(), m, getContext().system().dispatcher(), null);
			log.info("["+getSelf().path().name()+"] : I am waiting ...");
		}
		if (message instanceof MyMessage){
			MyMessage k = (MyMessage)message;
			if(k.data=="go"){
				MyMessage m =new MyMessage("yalla!");
				ref.tell(m,getSelf());
				log.info("["+getSelf().path().name()+"] send message "+m.data);

			}
		}
	
	}
}