Pattern 6:
Used 3 types of messages (MyMessage) | (MyMessageToRef) | (MyRefs)

Pattern 7:
Actor a sends 30 requests to b (without waiting for their corresponding responses) and receives 30 responses 

Pattern 8:
Actor a sends 30 requests to b (it waits for the corresponding responses) and receives 30 responses 
for that purpose and to create a future "ask" is used instead of "tell"

Pattern 9:
Done without using Pipe

Pattern 10:
The scheduler is used to wait (actor "a" waits) for the actors "b" and "c" to join before starting sending the message received by "a" to these two actor passing by the broadcaster.
The broadcaster receives the references of "b" and "c" beforehand

Pattern 11:
The actors "a", "b" and "c" join the merger and use the scheduler with a triggering message "go" before sending their messages to the merger which forward it to the actor "d". After each same message sent by the actor, one actor is discarded ("unjoined") each time starting from "c", "b" then "a".
A variable "i" is used to facilitate the creation of the messages of time hi+i(i can be 0,2,3.. it depends on the number of sent messages and therefore of the number of actors sending the messages since they are being discarded for each sent message) in order to make the algorithm more resilient.

Pattern 12:
In this pattern, in order to identify each group, the message containing the name of the group is parsed to extract the number following the world group and therefore to identify which group it is. Other method could have been set but to generalize the algorithm, this method seemed the most accurate.

Pattern 13:
The actor are each subscribed to at least one topic, each time a topic is triggered by a message, it sends back the message to the subscribed actors.

Pattern 14:
A list that contains the joined actor is created inside the laodbalancer.
To ensure the load balancing a variable i is created which changes each time a message is sent from the laodbalancer according to the following expression i = (i+1) % (list.size());

Pattern 15:
An actor is created than stopped once it receives the message "stop" from the actor system (as simple as that)

Pattern 16:
A session1 is created once the client1 wants to create a session by sending a message "createSession".It stays connected to it and sends messages as it wants( and receive the save message from the session1 each time"coucou", it could turn for example the received message but it wasn't specified in the requirements).As soon as the sessionManager receives the message "endSession" from the client1, it kills the session1.
The messages "createSession" and "endSession" are triggered respectively by the message "Connect" and "Disconnect" sent to the client1 by the actor system.

Pattern 17:
The maximum number of actors that will be create is specified by the actor system beforehand while creating the LoadBalancerActor. This pattern is pretty much similar to the pattern 14 expect that each time the LoadBalancerActor detects the word "finished" in the received message, it stops the concerned actor. 

Pattern 19:
A list of actor is sent and stored in each actor in order to create the adjacency matrix.

Pattern 20:
By creating a communication topology based on the presented adjacency matrix, an infinte cycle is created. the actor "d" can receive multiple messages from "b" but one message from "c".

Pattern 21:
In this Pattern two solutions are presented, one using sequence number and another alternative that works fine as well without using sequence number( it basically add a condition that "d" can forward a message to "e" only if it receives a message from both "b" and "c" which will break the infinite cycle since "d" receives only one message from "c").