package demo;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import scala.concurrent.Future;
import scala.concurrent.Await;

import akka.util.Timeout;
import java.time.Duration;
import akka.pattern.Patterns;

public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

	final Timeout timeout = Timeout.create(Duration.ofSeconds(5));


	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			return new FirstActor();
		});
	}




	static public class MyMessage {
		public final String data;
	
		public MyMessage(String data) {
			this.data = data;
		}
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof ActorRef){	
		ActorRef actorRef = (ActorRef) message;
		log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"]");
		for (int i=1;i<5;i++){
			MyMessage m = new MyMessage("Req number" + i);
			log.info("Actor {} send a req {} to the actor: {}",getSelf().path().name(), i,actorRef.path().name());
			Future<Object> future = Patterns.ask(actorRef,m, timeout);
			MyMessage res1 =(MyMessage) Await.result(future, timeout.duration());

		}
		}
	}
}


