package demo;

import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.actor.ActorRef;
import akka.event.LoggingAdapter;
import demo.Session1.MyMessage;
import demo.Session1.MyMessageAndRef;
public class sessionManager extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    public ActorRef session1= null;
	public sessionManager() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(sessionManager.class, () -> {
			return new sessionManager();
		});
	}

	
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof MyMessage){
            MyMessage m = (MyMessage)message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
            if (m.data.equals("createSession")){
                session1 = getContext().system().actorOf(Session1.createActor(), "session1");
                getSender().tell(session1,getSelf());

            }
            if (m.data.equals("endSession")){
                //will kill session1
                //session1 = null;
                getContext().system().stop(session1);
                log.info(getSelf().path().name()+ " killed the poor session");
            }
            
            
        }
	}

}
