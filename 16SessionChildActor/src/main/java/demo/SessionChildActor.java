package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.Session1.MyMessage;
import demo.Session1.MyMessageAndRef;

/**
 * @author Remi SHARROCK
 * @description
 */
public class SessionChildActor {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
	    final ActorRef client1 = system.actorOf(Client1.createActor(), "client1");
	    final ActorRef sessionManage = system.actorOf(sessionManager.createActor(), "sessionManage");

			// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
		MyMessage m = new MyMessage("Connect");
		MyMessageAndRef mm = new MyMessageAndRef(m,sessionManage);
		client1.tell(mm, ActorRef.noSender());
		sleepFor(5);
		MyMessage m1 = new MyMessage("Disconnect");
		MyMessageAndRef mm1 = new MyMessageAndRef(m1,sessionManage);
		client1.tell(mm1, ActorRef.noSender());
	    
	
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
    }
    public static void sleepFor(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
