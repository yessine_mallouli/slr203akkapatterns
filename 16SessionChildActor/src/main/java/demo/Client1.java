package demo;

import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.actor.ActorRef;
import akka.event.LoggingAdapter;
import demo.Session1.MyMessage;
import demo.Session1.MyMessageAndRef;
public class Client1 extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    public ActorRef ref = null;
	public Client1() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(Client1.class, () -> {
			return new Client1();
		});
	}

	

	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof MyMessageAndRef){
            MyMessageAndRef mm = (MyMessageAndRef) message;
            MyMessage m = mm.getMessage();
            ActorRef refer = mm.getActorRef();
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
            if (m.data.equals("Connect")){
                MyMessage m1 = new MyMessage("createSession");
                refer.tell(m1,getSelf());
            }
            if(m.data.equals("Disconnect")){
                ref = null;
                MyMessage m2 = new MyMessage("endSession");
                refer.tell(m2,getSelf());
            }
            
        }
        if (message instanceof ActorRef){
            ref =(ActorRef)message;
            log.info("["+getSelf().path().name()+"] is connected to ["+ ref.path().name() +"]");
            MyMessage mp1 = new MyMessage("firstMessage");
            ref.tell(mp1,getSelf());
            MyMessage mp2 = new MyMessage("secondMessage");
            ref.tell(mp2,getSelf());
            
        }
        if (message instanceof MyMessage){
            MyMessage mn = (MyMessage)message;
            log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+mn.data+"]");


        }
	}

}
