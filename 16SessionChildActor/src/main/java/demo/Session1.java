package demo;

import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.actor.ActorRef;
import akka.event.LoggingAdapter;
import demo.Session1.MyMessage;
import demo.Session1.MyMessageAndRef;
public class Session1 extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

	public Session1() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(Session1.class, () -> {
			return new Session1();
		});
	}

	static public class MyMessage {
		public final String data;
	
		public MyMessage(String data) {
			this.data = data;
		}
	  }
	static public class MyMessageAndRef {
		public final MyMessage m;
		public final ActorRef ref;
	
		public MyMessageAndRef(MyMessage m,ActorRef ref) {
			this.m = m;
			this.ref = ref;
		}
		public ActorRef getActorRef(){
			return ref;
		}
		public MyMessage getMessage(){
			return m;
		}
	  }

	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof MyMessage){
			MyMessage m = (MyMessage) message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
			MyMessage m1 =new MyMessage("coucou");
			getSender().tell(m1,getSelf());
		}
	}

}
