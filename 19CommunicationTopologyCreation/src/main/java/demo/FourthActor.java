package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.ArrayList;
import demo.FirstActor.List;
public class FourthActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);


	public FourthActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FourthActor.class, () -> {
			return new FourthActor();
		});
	}


	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof List){
			List list = (List)message;
			log.info("["+getSelf().path().name()+"] received list"+list+ " from ["+ getSender().path().name() +"]");		}
	}


}
