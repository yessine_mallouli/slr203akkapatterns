package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;

public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);


	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			return new FirstActor();
		});
	}
	public static class List implements Serializable{
		private static final long serialVersionUID = 1L;
		public final ArrayList<ActorRef> list;
		public List(ArrayList<ActorRef> list){
			this.list = list;
		}
		public void add(ActorRef ref){
			list.add(ref);
		} 
	}
	
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof List){
			List list = (List)message;
		log.info("["+getSelf().path().name()+"] received list"+list+ " from ["+ getSender().path().name() +"]");
		}
	}


}
