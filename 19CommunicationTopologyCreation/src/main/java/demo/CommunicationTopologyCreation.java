package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import java.util.ArrayList;
import demo.FirstActor.List;

/**
 * @author Remi SHARROCK and Axel Mathieu
 * @description Create an actor and passing his reference to
 *				another actor by message.
 */
public class CommunicationTopologyCreation {

	public static void main(String[] args) {
		// Instantiate an actor system
		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
	    final ActorRef a1 = system.actorOf(FirstActor.createActor(), "a1");
	    final ActorRef a2 = system.actorOf(SecondActor.createActor(), "a2");
		final ActorRef a3 = system.actorOf(ThirdActor.createActor(), "a3");
		final ActorRef a4 = system.actorOf(FourthActor.createActor(), "a4");
	    final ArrayList<ActorRef> list1 = new ArrayList<ActorRef>();
		final ArrayList<ActorRef> list2 = new ArrayList<ActorRef>();
		final ArrayList<ActorRef> list3 = new ArrayList<ActorRef>();
		final ArrayList<ActorRef> list4 = new ArrayList<ActorRef>();
		list1.add(a2);
		list1.add(a3);
		list2.add(a4);
		list3.add(a1);
		list3.add(a4);
		list4.add(a1);
		list4.add(a4);
		List llist1 = new List(list1);
		List llist2 = new List(list1);
		List llist3 = new List(list1);
		List llist4 = new List(list1);

		//ccreating the adjacency matrix
	    a1.tell(llist1, ActorRef.noSender());
	    a2.tell(llist2, ActorRef.noSender());
		a3.tell(llist3, ActorRef.noSender());
		a4.tell(llist4, ActorRef.noSender());
	
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
	}

}
