package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.time.Duration;
public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);


	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			return new FirstActor();
		});
	}
	static public class MyMessageToRef{
		public final MyMessage message;
		public final ActorRef ref;
		public MyMessageToRef(MyMessage message,ActorRef ref) {
			this.message = message;
			this.ref =ref;
		}
		public MyMessage getMyMessage(){
			return message;
		}
		public ActorRef getActorRef(){
			return ref;
		}
	}
	static public class MyMessage{
		public final String data;
		public MyMessage(String data) {
			this.data = data;
		}
		public String getMessage(){
			return data;
		}
		
	}


	
	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof MyMessageToRef){
            MyMessageToRef mr = (MyMessageToRef)message;
            MyMessage m = (MyMessage) mr.getMyMessage();
            ActorRef ref = (ActorRef) mr.getActorRef();
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
            ref.tell(m,getSelf());
			
		}
		if(message instanceof MyMessage){
			MyMessage m = (MyMessage)message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");	
		}
		
		
		
					
	}
	
	
}