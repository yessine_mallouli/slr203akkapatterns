package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.List;
import java.util.ArrayList;
import demo.FirstActor.MyMessage;
import demo.FirstActor.MyMessageToRef;
public class Topic2Actor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    public List<ActorRef> list = new ArrayList<ActorRef>();


	public Topic2Actor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(Topic2Actor.class, () -> {
			return new Topic2Actor();
		});
	}


	
	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof MyMessage){
			MyMessage m =(MyMessage)message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
            if (m.data.equals("Sub")){
                list.add(getSender());
                log.info(getSender().path().name() +" added");
            }
            if (m.data.equals("UnSub")){
                list.remove(getSender());
                log.info(getSender().path().name() +" removed");
            }
            if((getSender().path().name()=="publisher1")||(getSender().path().name()=="publisher2")){
            ActorRef ref = null;
            for (int i=0; i<list.size();i++){
                ref = (ActorRef)list.get(i);
                ref.tell(m,getSelf());
            }
        }
        }
        /*if (message instanceof MyMessageToRef){
            MyMessageToRef mr = (MyMessageToRef)message;
            MyMessage m = (MyMessage) mr.getMyMessage();
            ActorRef ref = (ActorRef) mr.getActorRef();
            if (m.data.equals("Sub")){
                list.add(ref);
                log.info(ref.path().name() +" added");
            }
            if(m.data.equals("UnSub")){
                list.remove(ref);
                log.info(ref.path().name() +" removed");

            }
        }*/
		
		
					
	}
	
	
}