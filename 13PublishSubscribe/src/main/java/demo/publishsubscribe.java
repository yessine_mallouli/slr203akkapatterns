package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.FirstActor.MyMessage;
import demo.FirstActor.MyMessageToRef;

/**
 * @author yessine MALLOULI
 * @description
 */
public class publishsubscribe {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first, transmitter and second actor 
		final ActorRef a = system.actorOf(FirstActor.createActor(), "a");
        final ActorRef b = system.actorOf(SecondActor.createActor(), "b");
		final ActorRef c = system.actorOf(ThirdActor.createActor(), "c");
		final ActorRef Topic1 = system.actorOf(Topic1Actor.createActor(), "Topic1");
        final ActorRef Topic2 = system.actorOf(Topic2Actor.createActor(), "Topic2");
		final ActorRef publisher1 = system.actorOf(publisher1Actor.createActor(), "publisher1");
		final ActorRef publisher2 = system.actorOf(publisher2Actor.createActor(), "publisher2");

			// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
			
		MyMessage mm1= new MyMessage("Sub");
		MyMessage mm2 = new MyMessage("hello");
		MyMessage mm3 = new MyMessage("world");
		MyMessageToRef m1 =new MyMessageToRef(mm1,Topic1);
		a.tell(m1,ActorRef.noSender());
		b.tell(m1,ActorRef.noSender());
		MyMessageToRef m2 =new MyMessageToRef(mm1,Topic2);
		b.tell(m2,ActorRef.noSender());
		c.tell(m2,ActorRef.noSender());
		sleepFor(2);
		MyMessageToRef m3 =new MyMessageToRef(mm2,Topic1);
		publisher1.tell(m3,ActorRef.noSender());
		MyMessageToRef m4 =new MyMessageToRef(mm3,Topic2);
		publisher2.tell(m4,ActorRef.noSender());
		sleepFor(1);
		MyMessage mm5 = new MyMessage("UnSub");
		MyMessageToRef m5 =new MyMessageToRef(mm5,Topic1);
		a.tell(m5,ActorRef.noSender());
		MyMessage mm6 = new MyMessage("hello2");
		MyMessageToRef m6 =new MyMessageToRef(mm6,Topic1);
		publisher1.tell(m6,ActorRef.noSender());



       
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
    }
    public static void sleepFor(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
