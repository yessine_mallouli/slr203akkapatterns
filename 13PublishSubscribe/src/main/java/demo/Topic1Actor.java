package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.List;
import java.util.ArrayList;
import demo.FirstActor.MyMessage;
import demo.FirstActor.MyMessageToRef;
public class Topic1Actor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    public List<ActorRef> list = new ArrayList<ActorRef>();


	public Topic1Actor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(Topic1Actor.class, () -> {
			return new Topic1Actor();
		});
	}


	
	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof MyMessage){
			MyMessage m =(MyMessage)message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
            if (m.data.equals("Sub")){
                list.add(getSender());
                log.info(getSender().path().name() +" added");
            }
            if (m.data.equals("UnSub")){
                list.remove(getSender());
                log.info(getSender().path().name() +" removed");
            }
            if((getSender().path().name()=="publisher1")||(getSender().path().name()=="publisher2")){
                ActorRef ref = null;
                for (int i=0; i<list.size();i++){
                    ref = (ActorRef)list.get(i);
                    ref.tell(m,getSelf());
                }
            }
        }
        
		
		
					
	}
	
	
}