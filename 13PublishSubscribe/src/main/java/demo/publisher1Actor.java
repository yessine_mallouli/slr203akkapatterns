package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;
import demo.FirstActor.MyMessageToRef;

public class publisher1Actor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);


	public publisher1Actor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(publisher1Actor.class, () -> {
			return new publisher1Actor();
		});
	}


	
	@Override
	public void onReceive(Object message) throws Throwable {
		
		if (message instanceof MyMessageToRef){
            MyMessageToRef mr = (MyMessageToRef)message;
            MyMessage m = (MyMessage) mr.getMyMessage();
            ActorRef ref = (ActorRef) mr.getActorRef();
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
            ref.tell(m,getSelf());
		}			
	}
}