package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;

public class mergerActor extends UntypedAbstractActor {


	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
   
    public ActorRef d;
    
    public mergerActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(mergerActor.class, () -> {
			return new mergerActor();
		});
	}
	
  
	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof ActorRef){
			d=(ActorRef)message;
			log.info("I was linked to actor reference [{}]", d.path().name());
		}
        if(message instanceof MyMessage){
            MyMessage m =(MyMessage)message;
            if ((getSender().path().name()=="c")&&(m.data=="hi")){
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
            d.tell(m,getSelf());
			}else if ((getSender().path().name()=="b")&&(m.data=="hi2")){
				log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
				d.tell(m,getSelf());
			}
			
        }
       
	}
	
	
}