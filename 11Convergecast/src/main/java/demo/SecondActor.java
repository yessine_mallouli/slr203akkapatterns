package demo;


import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;
import java.time.Duration;


public class SecondActor extends UntypedAbstractActor {

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public ActorRef ref;
	public int i=1;
	// Empty Constructor
	public SecondActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(SecondActor.class, () -> {
			return new SecondActor();
		});
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof ActorRef){
			ref = (ActorRef)message;
			MyMessage j = new MyMessage("join");
			ref.tell(j,getSelf());
			log.info("["+getSelf().path().name()+"] joined message ["+j.data+"]");
			int n=0;
			while(n<=1){
			MyMessage m = new MyMessage("go");
			getContext().system().scheduler()
			.scheduleOnce(Duration.ofMillis(3000), getSelf(), m, getContext().system().dispatcher(), null);
			log.info("["+getSelf().path().name()+"] : I am waiting ...");
			n++;
			
		}

		}
		if (message instanceof MyMessage){
			MyMessage k = (MyMessage)message;
			if((k.data=="go")&&(i<2)){
				MyMessage m =new MyMessage("hi");	
				ref.tell(m,getSelf());
				log.info("["+getSelf().path().name()+"] send message "+m.data);
				i++;
			
			}else{
				MyMessage m =new MyMessage("hi"+i);	
				log.info("["+getSelf().path().name()+"] send message "+m.data);
				ref.tell(m,getSelf());
				log.info("["+getSelf().path().name()+"] unjoins "+ref.path().name());

			}
			
		}
	}
	
	
}