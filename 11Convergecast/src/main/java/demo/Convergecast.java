package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.FirstActor.MyMessage;

/**
 * @author yessine MALLOULI
 * @description
 */
public class Convergecast {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first, transmitter and second actor 
		final ActorRef d = system.actorOf(FourthActor.createActor(), "d");
        final ActorRef a = system.actorOf(FirstActor.createActor(), "a");
		final ActorRef b = system.actorOf(SecondActor.createActor(), "b");
		final ActorRef c = system.actorOf(ThirdActor.createActor(), "c");
        final ActorRef merger = system.actorOf(mergerActor.createActor(), "merger");	    
			// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
		merger.tell(d,ActorRef.noSender());
		a.tell(merger,ActorRef.noSender());
		b.tell(merger,ActorRef.noSender());
		c.tell(merger,ActorRef.noSender());

       
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
    }
    public static void sleepFor(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
