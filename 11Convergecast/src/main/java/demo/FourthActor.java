package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;

public class FourthActor extends UntypedAbstractActor {


	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public ActorRef ref;

	public FourthActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(FourthActor.class, () -> {
			return new FourthActor();
		});
	}
	
	
	@Override
	public void onReceive(Object message) throws Throwable {
        
		if (message instanceof MyMessage){
			MyMessage m =(MyMessage)message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");

		}
	}
	
	
}