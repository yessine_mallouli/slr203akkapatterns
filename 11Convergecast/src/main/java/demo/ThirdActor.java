package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;
import java.time.Duration;


public class ThirdActor extends UntypedAbstractActor {


	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public ActorRef ref;

	public ThirdActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(ThirdActor.class, () -> {
			return new ThirdActor();
		});
	}
	
	/*public void changeRef(ActorRef actorRef){
		this.actorRef = actorRef;
		log.info("I was linked to actor reference {}", this.actorRef);
   }*/
  
	@Override
	public void onReceive(Object message) throws Throwable {
        if(message instanceof ActorRef){
			ref = (ActorRef)message;
			MyMessage j = new MyMessage("join");
			ref.tell(j,getSelf());
			log.info("["+getSelf().path().name()+"] joined message ["+j.data+"]");
			MyMessage m = new MyMessage("go");
			getContext().system().scheduler()
			.scheduleOnce(Duration.ofMillis(3000), getSelf(), m, getContext().system().dispatcher(), null);
			log.info("["+getSelf().path().name()+"] : I am waiting ...");

		}
		if (message instanceof MyMessage){
			MyMessage k = (MyMessage)message;
			if(k.data=="go"){
				MyMessage m =new MyMessage("hi");	
				log.info("["+getSelf().path().name()+"] send message "+m.data);
				ref.tell(m,getSelf());
				MyMessage u=new MyMessage("unjoin");
				log.info("["+getSelf().path().name()+"] unjoins "+ref.path().name());
			
			}
		}
	}
	
	
}