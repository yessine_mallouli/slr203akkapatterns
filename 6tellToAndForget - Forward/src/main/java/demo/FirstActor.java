package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

	public ActorRef b;
	public ActorRef tr;

	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			return new FirstActor();
		});
	}


	static public class MyMessage {
		public final String data;
	
		public MyMessage(String data) {
			this.data = data;
		}
	}
	
	static public class MyRefs {
		public final ActorRef a1;
		public final ActorRef a2;
		public MyRefs(ActorRef a1,ActorRef a2) {
			this.a1 = a1;
			this.a2 = a2;
		}
		public ActorRef firstactorGetter(){
			return a1;
		}
		public ActorRef secondactorGetter(){
			return a2;
		}
	}
	static public class MyMessageToRef {
		public final ActorRef a1;
		public final MyMessage m;
		public MyMessageToRef(ActorRef a1,MyMessage m) {
			this.a1 = a1;
			this.m = m;
		}
		public ActorRef actorGetter(){
			return a1;
		}
		public MyMessage messageGetter(){
			return m;
		}
	}
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof MyMessage){
			if( getSender().path().name()=="deadLetters"){
			MyMessage m = (MyMessage) message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
			MyMessage hm = new MyMessage("hello");
			MyMessageToRef mr = new MyMessageToRef(b,hm);
			//tr.changeRef(b);
			tr.tell(mr,getSelf());
		}else{
			MyMessage m = (MyMessage) message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
			log.info("done!");
		}
		}
		else if(message instanceof MyRefs){
			MyRefs refs = (MyRefs) message;
			tr = refs.firstactorGetter();
			b = refs.secondactorGetter();
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with actor references: ["+tr.path().name()+"] ["+b.path().name()+"]");

		}
		else {log.info("error somewhere!");
	}
	
	}
}