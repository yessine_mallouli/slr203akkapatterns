package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

	public ActorRef b;
	public ActorRef c;

	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			return new FirstActor();
		});
	}

	static public class MyMessage {
		public final String data;
	
		public MyMessage(String data) {
			this.data = data;
		}
	}
	
	static public class MyRefs {
		public final ActorRef a1;
		public final ActorRef a2;
		public MyRefs(ActorRef a1,ActorRef a2) {
			this.a1 = a1;
			this.a2 = a2;
		}
		public ActorRef firstactorGetter(){
			return a1;
		}
		public ActorRef secondactorGetter(){
			return a2;
		}
	}
	static public class MyMessageToRef {
		public final ActorRef a1;
		public final MyMessage m;
		public MyMessageToRef(ActorRef a1,MyMessage m) {
			this.a1 = a1;
			this.m = m;
		}
		public ActorRef actorGetter(){
			return a1;
		}
		public MyMessage messageGetter(){
			return m;
		}
	}


	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof MyRefs){
			MyRefs refs = (MyRefs) message;
			b = refs.firstactorGetter();
			c = refs.secondactorGetter();
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with actor references: ["+b.path().name()+"] ["+c.path().name()+"]");
			MyMessage m =new MyMessage("req1");
			MyMessageToRef mr = new MyMessageToRef(c,m);
			b.tell(mr, getSelf());
		}
	}


}
