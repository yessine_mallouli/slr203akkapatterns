package demo;

import akka.actor.Props;
import akka.actor.ActorRef;
import akka.actor.UntypedAbstractActor;
import demo.FirstActor.MyMessageToRef;
import demo.FirstActor.MyMessage;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class SecondActor extends UntypedAbstractActor {

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	// Empty Constructor
	public SecondActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(SecondActor.class, () -> {
			return new SecondActor();
		});
	}
	

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof MyMessageToRef)
		{
			MyMessageToRef mr = (MyMessageToRef)message;
			MyMessage m = mr.messageGetter();
			ActorRef c = mr.actorGetter(); 
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"] and [ref to "+c.path().name()+"]");		
			MyMessage mm = new MyMessage("res1");	
			c.tell(mm,getSelf());
		}

	}
	
	
}
