package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.multicasterActor.Group;
import demo.multicasterActor.MyMessage;

/**
 * @author yessine MALLOULI
 * @description
 */
public class multicast {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first, transmitter and second actor 
		final ActorRef receiver1 = system.actorOf(receiver1Actor.createActor(), "receiver1");
        final ActorRef receiver2 = system.actorOf(receiver2Actor.createActor(), "receiver2");
		final ActorRef receiver3 = system.actorOf(receiver3Actor.createActor(), "receiver3");
        final ActorRef multicaster = system.actorOf(multicasterActor.createActor(), "multicaster");	    
			// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
		Group grp1 = new Group("1",receiver1,receiver2);
		Group grp2 = new Group("2",receiver2,receiver3);
		multicaster.tell(grp1,ActorRef.noSender());
		multicaster.tell(grp2,ActorRef.noSender());
		MyMessage m1 =new MyMessage("hello to group1");
		multicaster.tell(m1,ActorRef.noSender());
		MyMessage m2 =new MyMessage("world to group2");
		multicaster.tell(m2,ActorRef.noSender());



       
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
    }
    public static void sleepFor(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
