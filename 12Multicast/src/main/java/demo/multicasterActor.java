package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

import java.io.Serializable;


public class multicasterActor extends UntypedAbstractActor {


	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
 
	public List<Group> list = new ArrayList<Group>();

	
    public multicasterActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(multicasterActor.class, () -> {
			return new multicasterActor();
		});
	}
	
	
  
   static public class MyMessage {
	public final String data;

	public MyMessage(String data) {
		this.data = data;
	}
	public String getMessage(){
		return data;
	}
}
static public class Group implements Serializable{
	private static final long serialVersionUID = 1L;
	public final String number;
	public final ActorRef ref1;
	public final ActorRef ref2;

	public Group(String number, ActorRef ref1, ActorRef ref2) {
		this.number = number;
		this.ref1 = ref1;
		this.ref2 = ref2;
	}
	public String getGroupNumber(){
		return number;
	}
	public ActorRef getRef1(){
		return ref1;
	}
	public ActorRef getRef2(){
		return ref2;
	}
	
}

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof Group){
			Group grp = (Group)message;
			list.add(grp);
			ActorRef refx = grp.getRef1();
			ActorRef refy = grp.getRef2();
			String num = grp.getGroupNumber();
			log.info("the multicastActor received group{} composed of references {} and {}", num,refx.path().name(),refy.path().name());
		}
        if(message instanceof MyMessage){
			MyMessage m =(MyMessage)message;
			String num = m.data.substring(m.data.length() - 1);
			String words[] = m.data.split(" ");
			int i =0;
			ActorRef ref1 = null ;
			ActorRef ref2 = null;
			Group groupElement = null;
			while(i<list.size()){
				groupElement=(Group)list.get(i);
				
				if(groupElement.getGroupNumber().equals(num)){
					
					break;
				}else{
					i++;
				}
				
			}
			
			if (i<=list.size()){
				ref1 = groupElement.getRef1();
				ref2 = groupElement.getRef2();
				ref1.tell(words[0],getSelf());
				ref2.tell(words[0],getSelf());
			
			}
			
			
        }
       
	}
	
	
}