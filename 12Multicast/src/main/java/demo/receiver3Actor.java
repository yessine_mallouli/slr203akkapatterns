package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class receiver3Actor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);


	public receiver3Actor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(receiver3Actor.class, () -> {
			return new receiver3Actor();
		});
	}


	
	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof String){
			String m =(String)message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m+"]");

		}
		
		
					
	}
	
	
}