package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;
import demo.FirstActor.MyMessageToRef;

public class transmitter extends UntypedAbstractActor {


	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	// Actor reference
	private ActorRef actorRef;
	public transmitter() {}
	public transmitter(ActorRef actorRef) {
		this.actorRef = actorRef;
		log.info("I was linked to actor reference {}", this.actorRef);
	}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(transmitter.class, () -> {
			return new transmitter();
		});
	}
	
	
	@Override
	public void onReceive(Object message) throws Throwable {
        if(message instanceof MyMessageToRef){
			MyMessageToRef mr = (MyMessageToRef)message;
			MyMessage m = mr.messageGetter();
			ActorRef b = mr.actorGetter(); 
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
			b.tell(m,getSender());
			//log.info("this is the new sender "+ getSelf().path().name());
		}
	}
	
	
}