package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;
import java.util.ArrayList;


public class LoadBalancerActor extends UntypedAbstractActor {


	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    public ArrayList<ActorRef> list= new ArrayList<ActorRef>();
    
    public int i=0;
    public LoadBalancerActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(LoadBalancerActor.class, () -> {
			return new LoadBalancerActor();
		});
	}

	@Override
	public void onReceive(Object message) throws Throwable {
        
        if (message instanceof MyMessage){
            MyMessage m = (MyMessage)message;
            if (m.data.equals("join")){
                log.info("add new reference "+getSender().path().name());
                list.add(getSender());
            }
            else if(m.data.equals("unjoin")){
                log.info("delete reference "+getSender().path().name());
                list.remove(getSender());
            }else{
                ActorRef ref =(ActorRef) list.get( i % (list.size()));
                log.info("["+getSelf().path().name()+ "] sends a message to ["+ref.path().name()+"]");
                i = (i+1) % (list.size());
                ref.tell(m,getSelf());
            }
        }
            
	}
	
	
}