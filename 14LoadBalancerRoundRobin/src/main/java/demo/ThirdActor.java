package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;
import demo.FirstActor.MyMessageToRef;

public class ThirdActor extends UntypedAbstractActor {


	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public ActorRef ref;

	public ThirdActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(ThirdActor.class, () -> {
			return new ThirdActor();
		});
	}
	
	/*public void changeRef(ActorRef actorRef){
		this.actorRef = actorRef;
		log.info("I was linked to actor reference {}", this.actorRef);
   }*/
  
	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof MyMessageToRef){
			MyMessageToRef mr =(MyMessageToRef) message;
			MyMessage m =(MyMessage)mr.messageGetter();
			if (m.data.equals("join")){
				ref = (ActorRef)mr.actorGetter();
				log.info("["+getSelf().path().name()+"] joined message "+m.data);
				ref.tell(m,getSelf());
			}
		}
		if (message instanceof MyMessage){
			MyMessage p =(MyMessage)message;
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+p.data+"]");

		}
	
	}
	
}