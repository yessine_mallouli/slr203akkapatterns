package demo;


import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;


public class SecondActor extends UntypedAbstractActor {

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public ActorRef ref;
	// Empty Constructor
	public SecondActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(SecondActor.class, () -> {
			return new SecondActor();
		});
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		
		if (message instanceof MyMessage){
			MyMessage p =(MyMessage)message;
			MyMessage m =new MyMessage(p.data+" finished");
			log.info(getSelf().path().name()+" sends a message ["+m.data+" ] to ["+getSender().path().name()+"]");
			getSender().tell(m,getSelf());
		}
			
	
	}
	
	
}