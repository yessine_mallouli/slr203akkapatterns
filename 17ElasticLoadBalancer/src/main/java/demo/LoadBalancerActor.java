package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import demo.FirstActor.MyMessage;
import java.util.ArrayList;


public class LoadBalancerActor extends UntypedAbstractActor {


	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    public ArrayList<ActorRef> list= new ArrayList<ActorRef>();
    private int max;
    public int i=0;
    public ActorRef b;
    public ActorRef c;
    public LoadBalancerActor() {}
    public LoadBalancerActor(int max){
        this.max = max;
        log.info("I am authorized to load balance between "+max+" actors");
    }
	// Static function that creates actor
	public static Props createActor(int max) {
		return Props.create(LoadBalancerActor.class, () -> {
			return new LoadBalancerActor(max);
		});
	}

	@Override
	public void onReceive(Object message) throws Throwable {
        
        if (message instanceof MyMessage){
            MyMessage m = (MyMessage)message;
            if((list.size()<max)&&(list.size()==0)){
                b =(ActorRef) getContext().system().actorOf(SecondActor.createActor(), "b");
                log.info("add new reference "+b.path().name());
                list.add(b);
                log.info("["+getSelf().path().name()+" send message ["+m.data+"] to ["+b.path().name()+"]");
                b.tell(m,getSelf());

            }else if((list.size()<max)&&(list.size()==1)){
                c =(ActorRef) getContext().system().actorOf(ThirdActor.createActor(), "c");
                log.info("add new reference "+c.path().name());
                list.add(c);
                log.info("["+getSelf().path().name()+"] send message ["+m.data+"] to ["+c.path().name()+"]");
                c.tell(m,getSelf());
            }else if(m.data.length()>8){
                if(m.data.substring(m.data.length()-8,m.data.length()).equals("finished")){
                if((getSender().path().name()=="b")&&(list.contains(b)))
                {   log.info("[b] is killed");
                    getContext().system().stop(b);}
                if((getSender().path().name()=="c")&&(list.contains(c)))
                
                {log.info("[c] is killed");
                getContext().system().stop(c);}
                }
            }else{
                ActorRef ref =(ActorRef) list.get( i % (list.size()));
                log.info("["+getSelf().path().name()+ "] sends a message to ["+ref.path().name()+"]");
                i = (i+1) % (list.size());
                ref.tell(m,getSelf());
            }
        }
            
	}
	
	
}