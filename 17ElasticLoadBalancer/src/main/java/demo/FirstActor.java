package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import java.time.Duration;
import akka.event.LoggingAdapter;

public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	public ActorRef ref;

	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			return new FirstActor();
		});
	}


	static public class MyMessage {
		public final String data;
	
		public MyMessage(String data) {
			this.data = data;
		}
	}
	
	static public class MyMessageToRef {
		public final ActorRef a1;
		public final MyMessage m;
		public MyMessageToRef(ActorRef a1,MyMessage m) {
			this.a1 = a1;
			this.m = m;
		}
		public ActorRef actorGetter(){
			return a1;
		}
		public MyMessage messageGetter(){
			return m;
		}
	}
	
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof ActorRef){
			ref = (ActorRef)message;
			MyMessage go = new MyMessage("go");
			getContext().system().scheduler()
			.scheduleOnce(Duration.ofMillis(1000), getSelf(), go, getContext().system().dispatcher(), null);
			log.info("["+getSelf().path().name()+"] : I am waiting ...");
		}
		if (message instanceof MyMessage){
			MyMessage k = (MyMessage)message;
			if(k.data=="go"){
				MyMessage m1 =new MyMessage("t1");
				ref.tell(m1,getSelf());
				log.info("["+getSelf().path().name()+"] send message "+m1.data);
				MyMessage m2 =new MyMessage("t2");
				ref.tell(m2,getSelf());
				log.info("["+getSelf().path().name()+"] send message "+m2.data);
				MyMessage m3 =new MyMessage("t3");
				ref.tell(m3,getSelf());
				log.info("["+getSelf().path().name()+"] send message "+m3.data);

			}
		}
	
	}
}