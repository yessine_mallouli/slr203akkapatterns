package demo;

import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.LoggingAdapter;
import akka.event.Logging;

public class Actor extends UntypedAbstractActor {

	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

	// Empty Constructor
	public Actor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(Actor.class, () -> {
			return new Actor();
		});

	}
	static public class MyMessage {
		public final String data;
	
		public MyMessage(String data) {
			this.data = data;
		}
	  }
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof MyMessage){
			MyMessage m =(MyMessage)message;
			if (m.data.equals("stop")){
				log.info("Actor is stopped!");
				getContext().system().stop(getSelf());

			}
		}
	}
	
	
}
