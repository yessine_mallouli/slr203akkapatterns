package demo;

import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import demo.FirstActor.MyMessage;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class SecondActor extends UntypedAbstractActor {

	public int i;
	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	// Empty Constructor
	public SecondActor() {}

	// Static function that creates actor
	public static Props createActor() {
		return Props.create(SecondActor.class, () -> {
			return new SecondActor();
		});
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof MyMessage){	
			i++;
			MyMessage actorRes = (MyMessage) message;
			MyMessage m =new MyMessage("Res number" + i);
			log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"]");
			log.info("{} send a res {} to the actor: {}",getSelf().path().name(),i, getSender().path().name());
			getSender().tell(m,getSelf());
			}
	}
	
	
}
