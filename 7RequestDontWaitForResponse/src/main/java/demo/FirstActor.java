package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class FirstActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	// Actor reference
	private ActorRef actorRef;

	public FirstActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(FirstActor.class, () -> {
			return new FirstActor();
		});
	}

	static public class MyMessage {
		public final String data;
	
		public MyMessage(String data) {
			this.data = data;
		}
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof ActorRef){	
		ActorRef actorRef = (ActorRef) message;
		log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"]");
		for (int i=1;i<=30;i++){
			MyMessage m = new MyMessage("Req number" + i);
			actorRef.tell(m,getSelf());
			log.info("{} send a req {} to the actor: {}",getSelf().path().name(), i,actorRef.path().name());

		}
		}
	}
}


